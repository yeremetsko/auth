import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/dist/vuetify.min.css';
// import VueI18n from 'vue-i18n';

Vue.use(Vuetify);
// Vue.use(VueI18n);

// const messages = {
//   en: {
//     $vuetify: {
//       dataTable: {
//         itemsPerPageText: 'Items per page:',
//         sortBy: 'Sort',
//         pageText: ''
//       },
//     },
//   },
//   ru: {
//     $vuetify: {
//       dataTable: {
//         itemsPerPageText: 'Элементов на странице:',
//         sortBy: 'Сортировать',
//         pageText: ''
//       },
//       dataFooter:{
//         pageText: '',
//         itemsPerPageAll: 'Все',
//         itemsPerPageOption: [1, 2, 3, -1]
//       },
//     },
//   },
// }

// const i18n = new VueI18n ({
//   locale: 'ru',
//   messages
// })


export default new Vuetify({
  // lang: {
  //   t: (key, ...params) => i18n.t(key, params)
  // }
});
