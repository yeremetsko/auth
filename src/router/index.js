import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
// import Auth from '../views/newAuthPage.vue';
import Auth from '../views/Auth.vue';
import firebase from 'firebase/app';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Auth',
    component: Auth
  },
  {
    path: '/login',
    name: 'Login',
    component: Auth
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      requireAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})
// router.beforeEach((to, from, next) => {
//   let currentUser = '';
//   const userUid = localStorage.getItem('userUid');
//   const requireAuth = to.matched.some(record => record.meta.requireAuth)
//   firebase.database().ref('users').orderByChild('uid').equalTo(userUid).on('child_added', (data) => {
//     currentUser = data.val().uid;
//   })
//   if (requireAuth && !currentUser) next('login');
//   else if (!requireAuth && currentUser) next('home');
//   next();
// })

router.beforeEach((to, from, next) => {
  const user = firebase.auth().currentUser;
  const requireAuth = to.matched.some(record => record.meta.requireAuth);
  if (requireAuth && !user) next('login')
  else if (!requireAuth && user) next('home')
  next();
})

export default router
