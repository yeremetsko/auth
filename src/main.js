import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import firebase from "firebase/app";
import "firebase/auth";
import router from './router'
import i18n from './i18n'
import '@fortawesome/fontawesome-free/css/all.css'
import store from './store'

Vue.config.productionTip = false

let firebaseConfig = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_DATABASE_URL,
  projectId: process.env.VUE_APP_PROJECT_ID,
  storageBucket: process.env.VUE_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_APPID
};

firebase.initializeApp(firebaseConfig);
let app = '';


firebase.auth().onAuthStateChanged(()=> {
  if (!app) {
    app = new Vue({
      vuetify,
      router,
      render: h => h(App),
      store,
      i18n
    }).$mount('#app')
  }
})
