import firebase from 'firebase'
import 'firebase/auth'
// import { currentUserRole } from './roleUser'

const currentUserRole = (userUid) => {
  let role = null
  firebase
    .database()
    .ref('users')
    .orderByChild('uid')
    .equalTo(userUid)
    .on('child_added', data => {
      role = data.val().role;
    })
  return role;
}

const loginUser = (username, password) => {
  let currentUser = null,
      role = null;
  firebase
    .auth()
    .signInWithEmailAndPassword(username, password)
    .then(data => {
      currentUser = data.user;
      role = currentUserRole(currentUser.uid);
    })
  return {currentUser, role, auth: true}
}

const signUpUser = (username, password) => {
  let login = null;
  firebase
    .auth()
    .createUserWithEmailAndPassword(username, password)
    .then( () => {
      login = loginUser(username, password);
    })
    return login;
}

const signOutUser = () => {
  firebase
    .auth()
    .signOut()
  return {currentUser: {}, role: null, auth: false}
}

export {loginUser, signUpUser, signOutUser}
