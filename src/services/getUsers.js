import firebase from 'firebase/app'

export const Users = () => {
  let users = [];
  firebase
    .database()
    .ref('users')
    .on('child_added', data => {
      users.push(data.val());
    })
  return users;
}
