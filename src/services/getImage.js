import firebase from 'firebase/app'

export const imageUrl = () => {
  return firebase
    .storage()
    .ref('/')
    .child('docks.jpg')
    .getDownloadURL()
    .catch(error => {
      console.log(error)
    })
}
