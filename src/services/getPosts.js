import firebase from 'firebase/app'



const firstPosts = () => {
  return firebase
    .database()
    .ref('posts')
    .orderByKey()
    .limitToLast(9)
    .once('value')
    .then(data => {
      let keys = Object.keys(data.val());
      let posts = keys.map(key => data.val()[key]).reverse();
      let key = keys[0];
      return {posts, key};
    })
}

const loadMorePosts = (prevKey) => {
  return firebase
    .database()
    .ref('posts')
    .orderByKey()
    .endAt(prevKey)
    .limitToLast(7)
    .once('value')
    .then(data => {
      let keys = Object.keys(data.val());
      let posts = keys.map(key => data.val()[key]).reverse().slice(1);
      let key = keys[0];
      return {posts, key};
    })
}

export {firstPosts, loadMorePosts}
