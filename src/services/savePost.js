import firebase from 'firebase/app'

export const savePost = (post) => {
  firebase
    .database()
    .ref('posts')
    .push({
      title: post.title,
      description: post.description,
      date: new Date().toLocaleString()
    })
}
