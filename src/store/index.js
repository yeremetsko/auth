import Vue from 'vue';
import Vuex from 'vuex';
import 'firebase/auth';
import auth from './models/auth';
import users from './models/users';
import posts from './models/posts'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    users,
    posts
  }
})
