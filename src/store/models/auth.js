import router from '../../router/index';
import {loginUser, signUpUser, signOutUser} from '../../services/auth';
// import {signUpUser} from '../../services/signUpUser';
import firebase from 'firebase/app'

const state = () => ({
  authUser: {
    currentUser: {},
    role: null,
    auth: null
  },
})

const getters = {
  currentUserAuth: state => {
    return state.authUser.auth;
  },
  isAdmin: state => {
    return state.authUser.role === 'admin' ? true : false;
  }
}

const mutations = {
  setAuthUser (state, payload) {
    state.authUser.currentUser = payload.currentUser;
    state.authUser.role = payload.role;
    state.authUser.auth = payload.auth;
  },
  setAuthStatus (state, payload) {
    state.authUser.auth = payload;
  }
}

const actions = {
  login ({ commit }, user) {
      commit('setAuthUser', loginUser(user.username, user.password));
      router.replace({name: 'Home'});
  },
  signUp({ commit }, user) {
    commit('setAuthUser', signUpUser(user.username, user.password));
    router.replace({name: 'Home'});
  },
  signOut({commit}) {
    commit('setAuthUser', signOutUser());
    router.replace({name: 'Login'});
  },
  authStatus({commit}) {
    const status = firebase.auth().currentUser;
    commit('setAuthStatus', status)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
