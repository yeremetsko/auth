import {firstPosts, loadMorePosts} from '../../services/getPosts';
import {savePost} from '../../services/savePost';
import {imageUrl} from '../../services/getImage';

const state = () => ({
  postsList: {
    posts: [],
    key: null
  },
  urlImage: null
})

const getters = {
  getPostsList: state => {
    return state.postsList.posts;
  },
  getUrlImage: state => {
    return state.urlImage;
  },
  getKey: state => {
    return state.postsList.key;
  }
}

const mutations = {
  addPost (state, payload) {
    state.postsList.posts.unshift(payload);
  },
  loadPosts (state, payload) {
    let newArray = state.postsList.posts.concat(payload.posts)
    state.postsList.posts = newArray;
    state.postsList.key = payload.key;
  },
  getAllPosts (state, payload) {
    state.postsList.posts = payload.posts;
    state.postsList.key = payload.key;
  },
  setImageUrl (state, payload) {
    state.urlImage = payload
  }
}

const actions = {
  async postsFromFirebase ({commit}) {
    let postsAndKey = await firstPosts();
    commit('getAllPosts', postsAndKey);
  },
  async loadPosts ({commit}, key) {
    let postsAndKey = await loadMorePosts(key);
    commit('loadPosts', postsAndKey);
  },
  addNewPost ({commit}, newPost) {
    savePost(newPost);
    commit('addPost', newPost);
  },
  async getImageUrlFromFirebase ({commit}) {
    const url = await imageUrl();
    commit('setImageUrl', url);
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
